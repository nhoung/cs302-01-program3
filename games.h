//Molina Nhoung
//CS302
//3/4/24
//
//Program 3
//This file will hold the inheritance hierarchy with Game being the base class and Card, Board, and Video
//being the derived classes. Each will have a common, Game, with personal information about
//the user. Each class will have similar functions, with something unique about each class.

#include <iostream>
#include <cstring>
#include <string>
#include <stdexcept>
#include <algorithm>
#include <cctype>
using namespace std;

class Game
{
	public:
		Game();		//default constructor
		Game(const string & a_name, const string & a_genre, const int an_age, const int a_rating);	//initialization list
		virtual ~Game();	//destructor
		friend istream & operator >> (istream & in, Game & ga2);	//input operator for the Game hierarchy
		friend ostream & operator << (ostream & out, Game & ga2);	//output operator for the Game hierarchy
		bool operator > (const Game & ga2) const;			//if current game is greater than or equal to incoming game
		bool operator == (const string & a_name) const;			//check if the name of the game is the same
		virtual int display(ostream & out) const = 0;			//pure virtual function to make Game an abtsract base class
		virtual int find_game(Game * to_find) const;			//compare data to see if the same
		virtual int age_appropriate() const;				//find out if the game is age appropriate, return 1 for true, 0 for false
		virtual int check_reviews() const;			//find out how the reviews are in %, return the % as an int
		virtual int check_genre() const;			//check if the genre matches the user's style, return success/failure
	protected:
		string name;
		string genre;
		int age;
		int rating;
		virtual int insert(istream & in);			//helper function for istream insert
		int display_helper(ostream & out) const;		//helper function for Game's display
};

class Video: public Game
{
	public:
		Video();	//default constructor
		Video(const string & a_name, const string & a_genre, const int an_age, const int a_rating, const string & an_equip, const int some_players); //initialization list
		~Video();	//destructor
		int display(ostream & out) const;		//display the equipment and player number
		int find_game(Game * to_find) const;		//compare data to see if the same
		int age_appropriate() const;			//find out if the game is age appropriate, return success/failure
		int check_reviews() const;		//find out how the reviews are in %, returns the % as an int
		int check_genre() const;		//check if the genre matches the user's style, return success/failure
		int check_equipment() const;		//check to see if the user has the right equipment for the game, return succes/failure, unique function for down-casting

	protected:
		int insert(istream & in);	//helper function for istream insert

	private:
		string equipment;
		int player;
};

class Board: public Game
{
	public:
		Board();	//default constructor
		Board(const string & a_name, const string & a_genre, const int an_age, const int a_rating, const int a_die, const int some_piece);	//initialization list
		~Board();	//destructor
		int display(ostream & out) const;		//display the dice and number of player pieces
		int find_game(Game * to_find) const;		//compare the data if they are the same
		int age_appropriate() const;			//find out if the game is age appropriate, return success/failure
		int check_reviews() const;			//find out how the reviews are in %, return the % as an int
		int check_genre() const;			//check if the genre matches the user's style, return succcess/failure

	protected:
		int insert(istream & in);	//helper function for istream insert

	private:
		int dice;
		int player_piece;
};

class Card: public Game
{
	public:
		Card();		//default constructor
		Card(const string & a_name, const string & a_genre, const int an_age, const int a_rating, const int a_deck, const int some_cards); //initialization list
		~Card();	//destructor
		int display(ostream & out) const;		//display the number of cards and what accessories for the card game
		int find_game(Game * to_find) const;		//comapre the data if they are the same
		int age_appropriate() const;			//find out if the game is age appropriate, return success/failure
		int check_reviews() const;		//find out how the reviews are in %, returns the % as an int
		int check_genre() const;		//check if the genre matches the user's style, return success/failure

	protected:
		int insert(istream & in);		//helper function for istream insert

	private:
		int deck;
		int cards;
};

int menu();		//menu function to help with organization
int game_menu();	//game menu to pick which game to insert
