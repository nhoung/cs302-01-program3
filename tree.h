//Molina Nhoung
//CS302
//3/4/24
//
//Program 3
//
//This file will have the node class and the tree class that will
//hold the abstract base class which would allow the
//data structor to hold more than one type of game. The
//tree will be a 2-3 tree where each node will hold
//at most 2 data items, with at most 3 children.

#include <memory>

class Node
{
	public:
		typedef shared_ptr <Node> node_ptr_type;		//shared pointer
		Node();						//default constructor
		Node(Game * new_game);				//initialization list
		~Node();					//destructor
		int set_left(node_ptr_type & new_left);				//return the left pointer
		int set_right(node_ptr_type & new_right);				//get the right pointer
		int set_parent(node_ptr_type & new_parent);				//get the parent pointer
		node_ptr_type & get_left();				//return the left pointer
		node_ptr_type & get_right();				//get the right pointer
		node_ptr_type & get_parent();				//get the parent pointer
		int set_color(int new_color);				//change the color of the node
		int display() const;				//display what's stored inside the node
		int find(Game * to_find) const;			//compare if the data is the same
		int retrieve(const string & to_find) const;	//compare the name of the game to retrieve
		Game * get_data() const;			//return the data
		int get_color() const;				//return the color
	private:
		Game * data;
		node_ptr_type left;
		node_ptr_type right;
		node_ptr_type parent;
		int color;					//1 = red, 0 = black
};


class Tree
{
	public:
		typedef shared_ptr <Node> node_ptr_type;	//shared ptr for the game
		Tree();					//default constructor
		~Tree();				//destructor
		int insert(Game * new_data);		//insert a new game into the list
		int display() const;			//display the entire tree
		int remove_all();			//remove the entire tree and deallocate the memory
		Game * retrieve(const string & to_find) const;	//retireve the node so the user can check requirements
	private:
		node_ptr_type root;
		int insert(node_ptr_type & root, Game * to_add); 	//BST insert
		//int insert(node_ptr_type & root, node_ptr_type & hold, Game * to_add);
		//node_ptr_type insert(node_ptr_type & root, Game * to_add);
		int display(const node_ptr_type & root) const;
		Game * retrieve(const node_ptr_type & root, const string & to_find) const;
		//RED-BLACK insertion
		/*node_ptr_type rotation(node_ptr_type & root);
		node_ptr_type right_rotate(node_ptr_type & root);
		node_ptr_type left_rotate(node_ptr_type & root);
		int left_rotate(node_ptr_type & root);
		int right_rotate(node_ptr_type & root);*/
};
