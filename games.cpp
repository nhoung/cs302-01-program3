//Molina Nhoung
//CS302
//3/4/24
//
//Program 3
//This file will implement the class functions in the hierarchy. The base class will have 
//virtual infront of the self-similar functions while the derived classes won't have
//virtual infront of the same functions. The functions will return value to be
//used by the client interface while also using arguments brought in by
//the client interface.

#include "games.h"

//GAME
//default constructor
Game::Game()
{
	age = 0;
	rating = 0;
}

//initialization list
Game::Game(const string & a_name, const string & a_genre, const int an_age, const int a_rating): name(a_name), genre(a_genre)
{
	age = an_age;
	rating = a_rating;
}

//destructor
Game::~Game()
{
	age = 0;
	rating = 0;
}

//output operator for the Game hierarchy
ostream & operator << (ostream & out, Game & ga2)
{
	ga2.display(out);
	return out;
}

//if current game is greater than or equal to incoming game
bool Game::operator > (const Game & ga2) const
{
	if (name.empty() || ga2.name.empty())
		return false;
	string a_copy;
	string a_copy2;
	a_copy = name;
	a_copy2 = ga2.name;
	transform(a_copy.begin(), a_copy.end(), a_copy.begin(), ::tolower);
	transform(a_copy2.begin(), a_copy2.end(), a_copy2.begin(), ::tolower);
	//if current name is greater than incoming name
	if (a_copy > a_copy2)
		return true;
	return false;
}

//check if the name of the game is the same
bool Game::operator == (const string & a_name) const
{
	if (name.empty() || a_name.empty())
		return 0;
	if (name == a_name)
		return 1;
	return 0;
}

//helper function for Game's display
int Game::display_helper(ostream & out) const
{
	if (name.empty() || genre.empty() || age == 0 || rating == 0)
		return 0;
	out << "\nName: " << name
	<< "\nGenre: " << genre
	<< "\nAge requirement: " << age
	<< "\nRating: " << rating << "%";
	return 1;
}

//calls the right game function to find a game, checks the incoming string with the name/genre to see if it matches
int Game::find_game(Game * to_find) const
{
	if (name.empty() || genre.empty() || age == 0 || rating == 0)
		return 0;
	if (name == to_find->name && genre == to_find->genre && age == to_find->age && rating == to_find->rating)
		return 1;
	return 0;
}

//find out if the game is age appropriate, return 1 for true, 0 for false
int Game::age_appropriate() const
{
	if (age == 0)
		return 0;
	cout << "\nAge required to play: " << age << endl;
	return 1;
}

//find out how the reviews are in %, return the % as an int
int Game::check_reviews() const
{
	if (rating == 0)
		return 0;
	cout << "\nGame's rating: " << rating << "%" << endl;
	return 1;
}

//check if the genre matches the user's style, return success/failure
int Game::check_genre() const
{
	if (genre.empty())
		return 0;
	cout << "\nGenre: " << genre << endl;
	return 1;
}

//VIDEOGAME
//default constructor
Video::Video()
{
	player = 0;
}

//initialization list
Video::Video(const string & a_name, const string & a_genre, const int an_age, const int a_rating, const string & an_equip, const int some_players): Game(a_name, a_genre, an_age, a_rating), equipment(an_equip)
{
	player = some_players;
}

//destructor
Video::~Video()
{
	player = 0;
}

//display the equipment and player number
int Video::display(ostream & out) const
{
	if (equipment.empty() || player == 0)
		return 0;
	Game::display_helper(out);
	out << "\nEquipment: " << equipment
	<< "\nNumber of players: " << player << endl;
	return 1;
}

int Video::find_game(Game * to_find) const
{
	if(!Game::find_game(to_find))
		return 0;
	if (equipment.empty() || player == 0)
		return 0;
	Video * vptr = dynamic_cast<Video*>(to_find);
	if (vptr)
	{
		if (equipment == vptr->equipment && player == vptr->player)
			return 1;
	}
	return 0;
}

//find out if the game is age appropriate, return success/failure
int Video::age_appropriate() const
{
	if (age == 0)
		return 0;
	cout << "\nAge required to play: " << age << endl;
	return 1;
}

//find out how the reviews are in %, returns the % as an int
int Video::check_reviews() const
{
	if (rating == 0)
		return 0;
	cout << "\nGame's rating: " << rating << "%" << endl;
	return 1;
}

//check if the genre matches the user's style, return success/failure
int Video::check_genre() const
{
	if (genre.empty())
		return 0;
	cout << "\nGenre: " << genre << endl;
	return 1;
}

//check to see if the user has the right equipment for the game, return succes/failure, unique function for down-casting
int Video::check_equipment() const
{
	if (equipment.empty())
		return 0;
	cout << "\nEquipment needed for this game: " << equipment << endl;
	return 1;
}


//BOARDGAME
//default constructor
Board::Board()
{
	dice = 0;
	player_piece = 0;
}

//initialization list
Board::Board(const string & a_name, const string & a_genre, const int an_age, const int a_rating, const int a_die, const int some_piece): Game(a_name, a_genre, an_age, a_rating)
{
	dice = a_die;
	player_piece = some_piece;
}

//destructor
Board::~Board()
{
	dice = 0;
	player_piece = 0;
}

//display the dice and number of player pieces
int Board::display(ostream & out) const
{
	Game::display_helper(out);
	out << "\nNumber of dice: " << dice
	<< "\nNumber of player pieces: " << player_piece << endl;
	return 1;
}

//compare the string to see if its a match
int Board::find_game(Game * to_find) const
{
	if (!Game::find_game(to_find))
		return 0;
	if (dice == 0 || player_piece == 0)
		return 0;
	Board * bptr = dynamic_cast<Board*>(to_find);
	if (bptr)
	{
		if (dice == bptr->dice && player_piece == bptr->player_piece)
			return 1;
	}
	return 0;
}

//find out if the game is age appropriate, return success/failure
int Board::age_appropriate() const
{
	if (age == 0)
		return 0;
	cout << "\nAge required to play: " << age << endl;
	return 1;
}

//find out how the reviews are in %, return the % as an int
int Board::check_reviews() const
{
	if (rating == 0)
		return 0;
	cout << "\nGame's rating: " << rating << "%" << endl;
	return 1;
}

//check if the genre matches the user's style, return succcess/failure
int Board::check_genre() const
{
	if (genre.empty())
		return 0;
	cout << "\nGenre: " << genre << endl;
	return 1;
}

//CARDGAME
//default constructor
Card::Card()
{
	deck = 0;
	cards = 0;
}

//initialization list
Card::Card(const string & a_name, const string & a_genre, const int an_age, const int a_rating, const int a_deck, const int some_cards): Game(a_name, a_genre, an_age, a_rating)
{
	deck = a_deck;
	cards = 0;
}

//destructor
Card::~Card()
{
	deck = 0;
}

//display the number of cards and what accessories for the card game
int Card::display(ostream & out) const
{
	if (deck == 0 || cards == 0)
		return 0;
	Game::display_helper(out);
	out << "\nDeck(s): " << deck
	<< "\nCards per deck: " << cards << endl;
	return 1;
}

//find the right game by comparing the string to a data member
int Card::find_game(Game * to_find) const
{
	if (!Game::find_game(to_find))
		return 0;
	if (deck == 0 || cards == 0)
		return 0;
	Card * cptr = dynamic_cast<Card*>(to_find);
	if (cptr)
	{
		if (deck == cptr->deck && cards == cptr->cards)
			return 1;
	}
	return 0;
}

//find out if the game is age appropriate, return success/failure
int Card::age_appropriate() const
{
	if (age == 0)
		return 0;
	cout << "\nAge required to play: " << age << endl;
	return 1;
}

//find out how the reviews are in %, returns the % as an int
int Card::check_reviews() const
{
	if (rating == 0)
		return 0;
	cout << "\nGame's rating: " << rating << "%" << endl;
	return 1;
}

//check if the genre matches the user's style, return success/failure
int Card::check_genre() const
{
	if (genre.empty())
		return 0;
	cout << "\nGenre: " << genre << endl;
	return 1;
}
