#compilor
CC = g++

.PHONY: clean
FLAGS = -std=c++17 -g -Wall -pedantic
OBJS = games.o main.o readin.o tree.o red-black_insert.o
COBJS = games.cpp main.cpp readin.cpp tree.cpp red-black_insert.cpp
CFLAGS = -Wall -pedantic
DFLAGS = -g

all: prog3

prog3: $(OBJS)
	$(CC) $(FLAGS) $(OBJS) -o prog3

main.o: main.cpp
	$(CC) $(FLAGS) -c main.cpp

games.o: games.cpp
	$(CC) $(FLAGS) -c games.cpp

readin.o: readin.cpp
	$(CC) $(FLAGS) -c readin.cpp

tree.o: tree.cpp
	$(CC) $(FLAGS) -c tree.cpp

red-black_insert.o: red-black_insert.cpp
	$(CC) $(FLAGS) -c red-black_insert.cpp

#Clean and zip
clean:
	rm -f *.o prog3

zip:
	zip ${USER}.zip Makefile games.cpp main.cpp red-black_insert.cpp readin.cpp tree.cpp games.h tree.h README.txt

