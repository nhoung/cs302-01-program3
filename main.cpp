//Molina Nhoung
//CS302
//3/4/24
//
//Program 3
//This program will implement a storage algorithm where users can input a game, with important
//info to keep track, or search through to see if the user's interests and equipment matches
//a game they could play. The user can display, remove, or check if game meets requirements.

#include "games.h"
#include "tree.h"

int main()
{
	//Variables
	Tree a_tree;
	int option {0};
	int game_option {0};
	string to_find;
	int check_option {0};

	//loop through until the user wants to stop
	do
	{
		//displays the menu options
		option = menu();
		switch(option)
		{
			//prompts the user for information to insert into the tree
			case 1:
			{
				game_option = game_menu();
				switch(game_option)
				{
					//insert information on a video game
					case 1:
					{
						//create a game pointer that has memory for the Video game
						Game * gptr = new Video;
						try
						{
							cin >> *gptr;
						}
						//catch if user entered nothing or if entered wrong data type
						catch (const exception & e)
						{
							cerr << "\nInvalid input: " << e.what() << endl;
						}
						if (!a_tree.insert(gptr))
							cout << "\nCannot insert information" << endl;
						break;
					}
					//insert information on a board game
					case 2:
					{
						//create a game pointer that has memory for the Board game
						Game * gptr = new Board;
						try
						{
							cin >> *gptr;
						}
						//catch if user entered nothng or if entered wrong data type
						catch (const exception & e)
						{
							cerr << "\nInvalid input: " << e.what() << endl;
						}
						if (!a_tree.insert(gptr))
							cout << "\nCannot insert information" << endl;
						break;
					}
					//insert information on a card game
					case 3:
					{
						//create a game pointer that has memory for the Card game
						Game * gptr = new Card;
						try
						{
							cin >> *gptr;
						}
						//catch if user entered nothing or if entered wrong data type
						catch (const exception & e)
						{
							cerr << "\nInvalid input: " << e.what() << endl;
						}
						if (!a_tree.insert(gptr))
							cout << "\nCannot insert information" << endl;
						break;
					}
					//exit this menu
					case 4:
					{
						break;
					}
					//other invalid input
					default:
					{
						cerr << "\nInvalid choice" << endl;
						break;
					}
				}
				break;
			}
			//find one game in the tree to return to the user and allows user to check for requirements
			case 2:
			{
				//prompt user
				cout << "\nWhat game do you want to find: ";
				getline(cin, to_find);
				//create a pointer to hold on to the game * being passed
				Game * gptr;
				gptr = a_tree.retrieve(to_find);
				//check if it's pointing to anything
				if (gptr)
				{
					//check if the incoming node pointer is a video game
					Video * vptr = dynamic_cast<Video*>(gptr);
					if (vptr)
					{
						do
						{
							cout << "\nWhat do you want to check"
								"\n\t1. Check age requirement"
								"\n\t2. Check rating (%)"
								"\n\t3. Check genre"
								"\n\t4. Check equipment"
								"\n\t5. Exit"
								"\nChoice: ";
							cin >> check_option;
							cin.clear();
							cin.ignore(100, '\n');
							switch(check_option)
							{
								//print age requirement
								case 1:
								{
									vptr->age_appropriate();
									break;
								}
								//print rating
								case 2:
								{
									vptr->check_reviews();
									break;
								}
								//print genre
								case 3:
								{
									vptr->check_genre();
									break;
								}
								//print equipment
								case 4:
								{
									vptr->check_equipment();
									break;
								}
								//exit out of the menu
								case 5:
								{
									break;
								}
								default:
								{
									cerr << "\nInvalid choice" << endl;
									break;
								}
							}
						} while (check_option != 5);
					}
					//only if it's not a video game
					else
					{
						do
						{
							cout << "\nWhat do you want to check"
								"\n\t1. Check age requirement"
								"\n\t2. Check rating (%)"
								"\n\t3. Check genre"
								"\n\t4. Exit"
								"\nChoice: ";
							cin >> check_option;
							cin.clear();
							cin.ignore(100, '\n');
							switch(check_option)
							{
								//print age requirement
								case 1:
								{
									gptr->age_appropriate();
									break;
								}
								//print rating
								case 2:
								{
									gptr->check_reviews();
									break;
								}
								//print genre
								case 3:
								{
									gptr->check_genre();
									break;
								}
								//exit out of the menu
								case 4:
								{
									break;
								}
								default:
								{
									cerr << "\nInvalid choice" << endl;
									break;
								}
							}
						} while (check_option != 4);
					}
					gptr = nullptr;
				}
				else
					cerr << "\nCould not find" << endl;
				break;
			}
			//displays the entire tree of games
			case 3:
			{
				if (!a_tree.display())
					cout << "\nEMPTY" << endl;
				break;
			}
			//remove everything
			case 4:
			{
				a_tree.remove_all();
				break;
			}
			//exit out of the program
			case 5:
			{
				cout << "\n***EXITING***" << endl << endl;
				break;
			}
			default:
			{
				cout << "\nInvalid choice, try again" << endl;
				break;
			}
		}
	} while (option != 5);

	return 0;
}


//user menu to insert, display, check if meet requirements, or remove the entire list
int menu()
{
	int option = 0;
	cout << "\n****GAME STORAGE****"
	"\n\t1. Insert"
	"\n\t2. Find game"
	"\n\t3. Display all games"
	"\n\t4. Remove all"
	"\n\t5. Exit"
	"\nChoice: ";
	cin >> option;
	cin.clear();
	cin.ignore(100, '\n');
	return option;
}

//menu for which game the user wants to enter into the tree
int game_menu()
{
	int option = 0;
	cout << "\nWhat game do you want to add"
	"\n\t1. Video Game"
	"\n\t2. Board Game"
	"\n\t3. Card Game"
	"\n\t4. Exit"
	"\nChoice: ";
	cin >> option;
	cin.clear();
	cin.ignore(100, '\n');
	return option;
}
