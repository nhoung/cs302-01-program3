This program will be using a Makefile to compile the code. To compile the code,
the command is make all, and will produce proj3 as the executable. To use this
executable, the command ./prog3 is used. Once the code is compiled and executed,
the user is greeted with a menu option where we they can test out inserting,
displaying, finding/retrieving, removing all, and simply exiting the program.
