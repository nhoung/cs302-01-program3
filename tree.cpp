//Molina Nhoung
//CS302
//3/4/24

//Program 3
//This file will implement the tree and node class for the hierarchy. The data structure is a red-black tree
//where the tree will be sorted by the name of the game. The search keys will be by name and genre. Since
//the node is pointing to the base class rather than having an object of the class, and because the base class
//is the hub for the derived classes, we are able to store different types instead of just one class.

#include "games.h"
#include "tree.h"

//NODE
//default constructor
Node::Node(): data(nullptr), left(nullptr), right(nullptr), parent(nullptr)
{
	color = 0;
}

//initialization list
Node::Node(Game * new_game): data(new_game), left(nullptr), right(nullptr), parent(nullptr)
{
	color = 0;
}

//destructor
Node::~Node()
{
	color = 0;
	if (data)
		delete data;
	data = nullptr;
}

//set the left pointer to a new node/nullptr
int Node::set_left(node_ptr_type & new_left)
{
	if (left == new_left)
		return 0;
	left = new_left;
	return 1;
}

//set the right pointer to a new node/nullptr
int Node::set_right(node_ptr_type & new_right)
{
	if (right == new_right)
		return 0;
	right = new_right;
	return 1;
}

//set the parent to a new parent/nullptr
int Node::set_parent(node_ptr_type & new_parent)
{
	if (parent == new_parent)
		return 0;
	parent = new_parent;
	return 1;
}

//return the left pointer
Node::node_ptr_type & Node::get_left()
{
	return left;
}

//get the right pointer
Node::node_ptr_type & Node::get_right()
{
	return right;
}

//get the parent pointer
Node::node_ptr_type & Node::get_parent()
{
	return parent;
}

//change the color of the node
int Node::set_color(int new_color)
{
	color = new_color;
	return color;
}

//return the data
Game * Node::get_data() const
{
	return data;
}

//return the color
int Node::get_color() const
{
	return color;
}

//display what's stored inside the node
int Node::display() const
{
	cout << *data;
	return 1;
}

//compare if the data is the same
int Node::find(Game * to_find) const
{
	if (data && data->find_game(to_find))
		return 1;
	return 0;
}

//compare the name of the game to retrieve
int Node::retrieve(const string & to_find) const
{
	if (!data)
		return 0;
	if (*data == to_find)
		return 1;
	return 0;
}

//RED-BLACK TREE
//default constructor
Tree::Tree(): root(nullptr)
{
}

//destructor
Tree::~Tree()
{
	remove_all();
}

//BST INSERT!!!!!
//insert a new game into the list
int Tree::insert(Game * new_data)
{
	if (!root)
	{
		shared_ptr<Node> temp(new Node(new_data));
		//temp.use_count(); if 1 it's good
		root = temp;
		return 1;
	}
	return insert(root, new_data);
}

int Tree::insert(node_ptr_type & root, Game * to_add)
{
	if (!root)
	{
		shared_ptr<Node> temp(new Node(to_add));
		root = temp;
		return 1;
	}
	//check for duplicates
	if (root->find(to_add))
	{
		delete to_add;
		to_add = nullptr;
		return 0;
	}
	if (*(root->get_data()) > *to_add)
		return insert(root->get_left(), to_add);
	else
		return insert(root->get_right(), to_add);
	return 0;
}

//displays the tree
int Tree::display() const
{
	if (!root)
		return 0;
	return display(root);
}

int Tree::display(const node_ptr_type & root) const
{
	if (!root)
		return 0;
	display(root->get_left());
	root->display();
	display(root->get_right());
	return 1;
}

//removes the entire tree
int Tree::remove_all()
{
	root = nullptr;
	return 1;
}

//retireve the node so the user can check requirements
Game * Tree::retrieve(const string & to_find) const
{
	if (!root)
		return 0;
	return retrieve(root, to_find);
}

Game * Tree::retrieve(const node_ptr_type & root, const string & to_find) const
{
	if (!root)
		return 0;
	Game * temp = nullptr;
	if (root->retrieve(to_find))
		return root->get_data();
	//if left is holding anything
	if ((temp = retrieve(root->get_left(), to_find)))
		return temp;
	//then go to the right to find if it holds anything
	else
		return temp = retrieve(root->get_right(), to_find);
}
