//Molina Nhoung
//CS302
//3/4/24

//Program 3
//This file will implement the functions that read in the data for each class in
//the hierarchy. The derived classes will kickstart the base class insertion so that each
//different type of games can be stored in one single tree with the base class being held in the nodes.

#include "games.h"

//GAMES
//input operator for the Game class to call hierarchy insert function depending on the class pointer type
istream & operator >> (istream & in, Game & ga2)
{
	ga2.insert(in);
	return in;
}

//helper function for istream insert
int Game::insert(istream & in)
{
	string get_name;
	string get_genre;
	int get_age;
	int get_rating;
	//prompt the user for the name of the game
	cout << "\nName of the game: ";
	getline(in, get_name);
	//check if the user entered anything, throw and exception if they didn't
	if (get_name.empty())
		throw runtime_error("Entered nothing");
	name.clear();
	name = get_name;
	
	//prompt the user for the genre of the game
	cout << "\nGame's genre: ";
	getline(in, get_genre);
	//check if the user entered anything, throw and exception if they didn't
	if (get_genre.empty())
		throw runtime_error("Entered nothing");
	genre.clear();
	genre = get_genre;

	//prompt the user for the age requirement
	cout << "\nMinimum age to play: ";
	cin >> get_age;
	//check if the user entered a character
	if (in.fail())
	{
		in.clear();
		in.ignore(100, '\n');
		throw invalid_argument("Did not enter a number");
	}
	in.clear();
	in.ignore(100, '\n');
	//check if the user entered anything, throw an exception if they didn't
	if (get_age == 0)
		throw runtime_error("Entered nothing");
	age = get_age;

	//prompt the user for the ratings of the game %
	cout << "\nGame's rating (1-100): ";
	cin >> get_rating;
	//check if the user entered a character
	if (in.fail())
	{
		in.clear();
		in.ignore(100, '\n');
		throw invalid_argument("Did not enter a number");
	}
	in.clear();
	in.ignore(100, '\n');
	//check if the user entered anything, throw an exception if they didn't
	if (get_rating == 0)
		throw runtime_error("Entered nothing");
	rating = get_rating;
	return 1;
}

//VIDEOGAME
//helper function for istream insert, passes in the istream to insert into
int Video::insert(istream & in)
{
	Game::insert(in);
	string get_equip;
	int get_player = 0;
	//prompt the user for the game's equipment
	cout << "\nEquipment needed: ";
	getline(in, get_equip);
	//check if the user entered anything, throw an exception if they didn't
	if (get_equip.empty())
		throw runtime_error("Entered nothing");
	equipment.clear();
	equipment = get_equip;

	//prompt the user for the number of players needed for the game
	cout << "\nHow many players can play the game: ";
	in >> get_player;
	//check if the user entered a character
	if (in.fail())
	{
		in.clear();
		in.ignore(100, '\n');
		throw invalid_argument("Did not enter a number");
	}
	in.clear();
	in.ignore(100, '\n');
	//check if the user entered anything, throw an exception if they didn't
	if (get_player == 0)
		throw runtime_error("Entered nothing");
	player = get_player;
	return 1;
}


//BOARDGAME
//helper function for istream insert
int Board::insert(istream & in)
{
	Game::insert(in);
	int get_dice;
	int get_piece;
	//prompt the user for the number of dice
	cout << "\nHow many dice: ";
	in >> get_dice;
	//check if the user entered a character
	if (in.fail())
	{
		in.clear();
		in.ignore(100, '\n');
		throw invalid_argument("Did not enter a number");
	}
	in.clear();
	in.ignore(100, '\n');
	dice = get_dice;

	//prompt the user for the number of player pieces
	cout << "\nHow many player pieces: ";
	in >> get_piece;
	//check if the user entered a character
	if (in.fail())
	{
		in.clear();
		in.ignore(100, '\n');
		throw invalid_argument("Did not enter a number");
	}
	in.clear();
	in.ignore(100, '\n');
	player_piece = get_piece;
	return 1;
}

//helper function for istream insert
int Card::insert(istream & in)
{
	Game::insert(in);
	int get_deck = 0;
	int get_cards = 0;
	//prompt the user for the number of decks
	cout << "\nHow many deck of cards: ";
	in >> get_deck;
	//check if the user entered a character
	if (in.fail())
	{
		in.clear();
		in.ignore(100, '\n');
		throw invalid_argument("Did not enter a number");
	}
	in.clear();
	in.ignore(100, '\n');
	//check if the user entered anything, throw an exception if they didn't
	if (get_deck == 0)
		throw runtime_error("Entered nothing");
	deck = get_deck;

	//prompt the user for the number of cards per deck
	cout << "\nHow many cards per deck: ";
	in >> get_cards;
	//check if the user entered a character
	if (in.fail())
	{
		in.clear();
		in.ignore(100, '\n');
		throw invalid_argument("Did not enter a number");
	}
	in.clear();
	in.ignore(100, '\n');
	//check if the user entered anything, throw an exception if they didn't
	if (get_cards == 0)
		throw runtime_error("Entered nothing");
	cards = get_cards;
	return 1;
}

