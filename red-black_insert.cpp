//Molina Nhoung
//302
//3/3/24

//Program 3
//This file will implement the red-black tree insertion algorithm, dealing with each case.
//Two red nodes cannot be connected, has to alternate red-black, but two black nodes can be
//connected. The red-black tree will help with keeping the tree more balanced compared to a
//binary search tree. Red is denoted by 1, black is 0
/*
#include "games.h"
#include "tree.h"

//wrapper for the insertion to check if the list is empty
int Tree::insert(Game * new_data)
{
	//check if list is empty
	if (!root)
	{
		//root is already black
		shared_ptr<Node> temp(new Node(new_data));
		root = temp;
		return 1;
	}
	return insert(root, new_data);
}

//insert a new node normally, making the node red
int Tree::insert(node_ptr_type & root, node_ptr_type & hold, Game * to_add)
{
	if (!root)
	{
		//shared_ptr<Node> temp(new Node(to_add));
		node_ptr_type temp = make_shared<Node>(to_add);
		root = temp;
		hold = temp;
		//change leaf color to red
		root->set_color(1);
		return 1;
	}
	//check for duplicates
	if (root->find(to_add))
	{
		delete to_add;
		to_add = nullptr;
		return 0;
	}
	//check if current data is greater than incoming data, then recurse left
	if (*(root->get_data()) > *to_add)
	{
		//connect the tree as we go down
//		if (root->get_parent())
//			root->get_parent()->set_left(root);
		insert(root->get_left(), to_add);
		root->get_left()->set_parent(root);
	}
	else
	{
		//connect the tree as we go down
		//if (root->get_parent())
		//	root->get_parent()->set_right(root);
		insert(root->get_right(), to_add);
		root->get_right()->set_parent(root);
	}
	//if no grandparent, return
	if (root->get_parent())
	{
		if (!(root->get_parent()->get_parent()))
				return 1;
	}
	rotation(root);
	return 1;
}

//perform rotations to balance the tree after insertion
Node::node_ptr_type Tree::rotation(node_ptr_type & root)
{
	if (!root)
		return 0;
	if (!root->get_parent())
		return 0;
	//check if parent is red
	if (root->get_parent()->get_color() == 0)
	{
		
	//check if root's parents right is pointing to root
	if (root->get_parent()->get_right() == root)
	{
		//if root and root's parent is red
		if (root->get_color() == 1 && root->get_parent()->get_color() == 1)
		{
			//check if parent has left child and check if its black CHANGE THIS
			if (!(root->get_parent()->get_left()) || root->get_parent()->get_left()->get_color() == 0)
			{
				//check if root has a left and if root's left's color is red
				if (root->get_left() && root->get_left()->get_color() == 1)
				{
					//do a right-left rotation, rotate root's right first
					//node_ptr_type temp = root->get_right();
					node_ptr_type temp = right_rotate(root->get_right());
					root->set_right(temp);
					root->get_right()->set_parent(root);
					//rotate root
					root = left_rotate(root);
					//set root to black
					root->set_color(0);
					//set root's left to black
					root->get_left()->set_color(1);
				}
				//check if root has a right and if it is red
				else if (root->get_right() && root->get_right()->get_color() == 1)
				{
					//do a left-left rotation, rotate root
					root = left_rotate(root);
					root->set_color(0);
					root->get_left()->set_color(1);
				}
			}
			else
			{
				root->get_parent()->get_left()->set_color(0);
				root->set_color(0);
				//if root's parent is not at the real root, change it to red
				if (root->get_parent() != this->root)
					root->get_parent()->set_color(1);
			}
		}
		else
		{
			//check root's parent doesnt have a right or if that right is black
			if (!(root->get_parent()->get_right()) || root->get_parent()->get_right()->get_color() == 0)
			{
				//check root left and if the color is red
				if (root->get_left() && root->get_left()->get_color() == 1)
				{
					root = right_rotate(root);
					root->set_color(0);
					root->get_left()->set_color(1);
				}
				//check root right and if color is red
				else if (root->get_right() && root->get_right()->get_color() == 1)
				{
					node_ptr_type temp = left_rotate(root->get_left());
					root->set_left(temp);
					root->get_left()->set_parent(root);
					root = right_rotate(root);
					root->set_color(0);
					root->get_right()->set_color(1);
				}
			}
			else
			{
				root->get_parent()->get_right()->set_color(0);
				root->set_color(0);
				//if root's parent is not at the real root, change it to red
				if (root->get_parent() != this->root)
					root->get_parent()->set_color(1);
			}
		}
	}
	return 0;
}

//perform a right rotation with child and parent, pass in the parent and return child
//Node::node_ptr_type Tree::right_rotate(node_ptr_type & root)
int Tree::right_rotate(node_ptr_type & root)
{
	//if incoming node is empty
	if (!root)
		return 0;
	//create temperary nodes to move them around
//	shared_ptr<Node> lchild = root->get_left();
//	shared_ptr<Node> lrchild = lchild->get_right();
//	lchild->set_right(root);
//	root->set_left(lrchild);
//	root->set_parent(lchild);
//	if (lrchild)
//		lrchild->set_parent(root);
//	return lchild;
	node_ptr_type lchild = root->get_left();
	root->set_left(lchild->get_right());
	if (lchild->get_right())
		lchild->get_right->set_parent(root);
	lchild->set_parent(root->get_parent());
	if (root->get_parent() == nullptr)
		this->root = lchild;
	else if (root == root->get_parent()->get_right())
		root->get_parent()->set_right(lchild);
	else
		root->get_parent()->set_left(lchild);
	lchild->set_right(root);
	root->set_parent(lchild);
	return 1;
}

//perform a left rotation with child and parent, pass in the parent and return the child
//Node::node_ptr_type Tree::left_rotate(node_ptr_type & root)
int Tree::left_rotate(node_ptr_type & root)
{
	//if incoming node is empty
	if (!root)
		return 0;
	//create temperary nodes to move them around
//	shared_ptr<Node> rchild = root->get_right();
//	shared_ptr<Node> rlchild = rchild->get_left();
//	rchild->set_left(root);
//	root->set_right(rlchild);
//	root->set_parent(rchild);
//	if (rlchild)
//		rlchild->set_parent(root);
//	return rchild;
	node_ptr_type rchild = root->get_right();
	root->set_right(rchild->get_left());
	if (rchild->get_left())
		rchild->get_left()->set_parent(root);
	rchild->set_parent(root->get_parent());
	if (root->get_parent() == nullptr)
		this->root = rchild;
	else if (root == root->get_parent()->get_left())
		root->get_parent()->set_left(rchild);
	else
		root->get_parent()->set_right(rchild);
	rchild->set_left(root);
	root->set_parent(rchild);
	return 1;
}
*/
